package org.jlu.dede.publicUtlis.json;

import lombok.Data;
import org.springframework.http.HttpStatus;
import com.netflix.hystrix.exception.HystrixBadRequestException;

@Data
public class RestException extends HystrixBadRequestException {
   HttpStatus status;
   Integer errorCode;
    public RestException(HttpStatus status, String message) {
        super(message);
        this.status = status;
    }
    public RestException(Integer errorCode, String message) {
        super(message);
        this.status =HttpStatus.valueOf(418);
        this.errorCode=errorCode;
    }

    public RestException(HttpStatus status) {
        super(status.getReasonPhrase());
        this.status = status;
    }

    public RestException(int status) {
        this(HttpStatus.valueOf(status));
    }

    public RestException(String msg) {
        super(msg);
        this.status = HttpStatus.I_AM_A_TEAPOT;
    }

    public RestException(Exception e) {
        super(e.getMessage());
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
    }
}
