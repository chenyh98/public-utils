package org.jlu.dede.publicUtlis.json;

import org.springframework.http.HttpStatus;

public class AuthFailException extends RestException {
    public AuthFailException(String msg) {
        super(msg);
        status = HttpStatus.UNAUTHORIZED;
    }
}
