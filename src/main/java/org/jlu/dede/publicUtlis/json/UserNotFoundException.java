package org.jlu.dede.publicUtlis.json;

import org.springframework.http.HttpStatus;


public class UserNotFoundException extends RestException {
    public UserNotFoundException(String email) {
        super(HttpStatus.NOT_FOUND, "User with email" + email + " not found.");
    }

    public UserNotFoundException(String email, String password) {
        super(HttpStatus.FORBIDDEN, "Login for " + email + " error, wrong email or password.");
    }
}
