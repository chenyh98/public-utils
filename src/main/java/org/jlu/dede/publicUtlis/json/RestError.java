package org.jlu.dede.publicUtlis.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RestError {
    Integer errorCode;
    String message;
}
