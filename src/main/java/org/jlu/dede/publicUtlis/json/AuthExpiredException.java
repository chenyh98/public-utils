package org.jlu.dede.publicUtlis.json;


public class AuthExpiredException extends AuthFailException {
    public AuthExpiredException(String email) {
        super("Authentication for " + email + " has expired.");
    }
}
