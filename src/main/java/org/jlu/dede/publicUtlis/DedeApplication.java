package org.jlu.dede.publicUtlis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DedeApplication {

    public static void main(String[] args) {
        SpringApplication.run(DedeApplication.class, args);
    }

}
