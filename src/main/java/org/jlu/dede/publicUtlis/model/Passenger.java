package org.jlu.dede.publicUtlis.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;


@Data
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class,property="id")
public class Passenger {
    Integer id;
    String x;
    String y;
    String certificate;;


    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
    Account account;
    Order order;
    String homeAddress;
    String companyAddress;
    String commonAddressOne;
    String commonAddressTwo;
}

