package org.jlu.dede.publicUtlis.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;


import java.math.BigDecimal;


@Data
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class,property="id")
public class Order {
    Integer id;
    Integer type;//1：快车  2：非预约的专车  3:预约的专车
    Passenger passenger;//乘客外键

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    Driver driver;//司机外键

    public Record getRecord() {
        return record;
    }

    public void setRecord(Record record) {
        this.record = record;
    }

    String createTime;
    String acceptTime;
    String driverArriveTime;
    String departTime;
    String arriveDestinationTime;
    String payTime;
    double vetifyValue;
    BigDecimal finalMoney;
    String idealDepartSite;
    String idealDestinationSite;
    String actualDepartSite;
    String actualDestinationSite;
    Integer state;//0：被取消订单（历史订单）   1：发起预约订单  2：预约订单等待开始   3：发起当前订单   4：司机接收订单    5：开始订单   6：待支付   7：已支付（历史订单）
    String startTime;
    String orderTime;
    double distance;
    Record record;//交易记录外键
}