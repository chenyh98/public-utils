package org.jlu.dede.publicUtlis.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Account {
    Integer id;
    String name;
    Integer age;
    String sex;
    String password;
    String phone;
    String email;
    String registerTime;
    double verifyValue;
    BigDecimal money;
    Integer type;//1:乘客   2：司机
    Integer state;//0：未登录  1：登陆

}
