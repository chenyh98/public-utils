package org.jlu.dede.publicUtlis.model;

import lombok.Data;


@Data
public class BlackList {
    Integer id;

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    Passenger passenger;//乘客外键
    Driver driver;//司机外键
}