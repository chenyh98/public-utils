package org.jlu.dede.publicUtlis.model;

import java.util.List;

public class Push {
    public Push(){}
    public Push(String alias, String notificationTitle, String notificationContent, String msgContent, String objType, Object obj)
    {
        this.alias = alias;
        this.notificationTitle = notificationTitle;
        this.notificationContent = notificationContent;
        this.msgContent = msgContent;
        this.objType = objType;
        this.obj = obj;
    }
    public Push(String alias, String notificationTitle, String notificationContent)
    {
        this.alias = alias;
        this.notificationTitle = notificationTitle;
        this.notificationContent = notificationContent;
    }
    public Push(String alias, String msgContent, String objType, Object obj)
    {
        this.alias = alias;
        this.msgContent = msgContent;
        this.objType = objType;
        this.obj = obj;
    }
    public Push(List<String> aliases, String notificationTitle, String notificationContent, String msgContent, String objType, Object obj)
    {
        this.aliases = aliases;
        this.notificationTitle = notificationTitle;
        this.notificationContent = notificationContent;
        this.msgContent = msgContent;
        this.objType = objType;
        this.obj = obj;
    }
    public Push(List<String> aliases, String notificationTitle, String notificationContent)
    {
        this.aliases = aliases;
        this.notificationTitle = notificationTitle;
        this.notificationContent = notificationContent;
    }
    public Push(List<String> aliases, String msgContent, String objType, Object obj)
    {
        this.aliases = aliases;
        this.msgContent = msgContent;
        this.objType = objType;
        this.obj = obj;
    }

    private String alias;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public void setAliases(List<String> aliases) {
        this.aliases = aliases;
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public String getNotificationContent() {
        return notificationContent;
    }

    public void setNotificationContent(String notificationContent) {
        this.notificationContent = notificationContent;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public String getObjType() {
        return objType;
    }

    public void setObjType(String objType) {
        this.objType = objType;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    private List<String> aliases;
    private String notificationTitle;
    private String notificationContent;
    private String msgContent;
    private String objType;
    private Object obj;

}
