package org.jlu.dede.publicUtlis.model;

import lombok.Data;

@Data
public class Location {
    String X;
    String Y;
}
