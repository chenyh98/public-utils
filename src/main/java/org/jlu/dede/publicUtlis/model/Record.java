package org.jlu.dede.publicUtlis.model;

import lombok.Data;


import java.math.BigDecimal;


@Data
public class Record {
    Integer id;
    Account payer;
    Account accepter;

    public Account getPayer() {
        return payer;
    }

    public void setPayer(Account payer) {
        this.payer = payer;
    }

    public Account getAccepter() {
        return accepter;
    }

    public void setAccepter(Account accepter) {
        this.accepter = accepter;
    }

    BigDecimal money;
    String tradeTime;
    Integer type;//0：充值   1：提现    2：交易
}
