package org.jlu.dede.publicUtlis.model;

import lombok.Data;


@Data
public class Car {
    Integer id;
    String num;
    String color;
    String type;//1：快车  2：专车
    Integer carLoad;
    Driver driver;//车主外键

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }
}
